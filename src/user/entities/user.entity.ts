import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  login: string;

  @Column()
  name: string;

  @Column()
  password: string;

  @CreateDateColumn()
  createdAT: Date;

  @UpdateDateColumn()
  updatedAT: Date;

  @DeleteDateColumn()
  deletedAT: Date;
}
